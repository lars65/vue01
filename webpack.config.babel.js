import path from 'path'
import webpack from 'webpack'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import WebpackShellPlugin from 'webpack-shell-plugin'

module.exports = {
  entry: './src/mainy.js',
  output: {
    path: path.resolve(__dirname, './img'),
    publicPath: '/img/',
    filename: 'build.js' // build.js => index.pug => index.html => #APP => App.vue
  },
  watch: true,
  module: {
    rules: [
      //{ test: /\.vue$/, enforce: 'pre', exclude: /node_modules/, loader: 'jshint-loader', options: { camelcase: true, emitErrors: true, failOnHint: false, esversion:6 }, },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {}
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader!jshint-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      },
      {
        test: /\.pug$/,
        exclude: /node_modules/,
        loader: "pug-loader?pretty=true"
      },
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'based on jade template',
      //filename: 'PUG-template-based.html',
      template: './src/index.pug'
    }),
    new WebpackShellPlugin({
      onBuildStart: ['echo ">>>>>>>>>>>>>>>> Starting <<<<<<<<<<<<<<<<<<<<<<<" && pwd'],
      onBuildEnd: ['sh in2.sh']
    })
  ],
  devServer: {
    hot: true,
    historyApiFallback: true,
    //noInfo: true,
  },
  devtool: '#inline-source-map',
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new WebpackShellPlugin({
      onBuildStart: ['echo ">>>>>>>>>>>>>>>> Starting <<<<<<<<<<<<<<<<<<<<<<<" && pwd'],
      onBuildEnd: ['sh in2.sh']
    })
  ])
}
