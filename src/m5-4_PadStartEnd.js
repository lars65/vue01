let rideName = "Scooby Doo";

let startPaddedName = rideName.padStart(9, "xo");
console.log(startPaddedName);

let endPaddedName = rideName.padEnd(15, "x");
console.log(endPaddedName);