  /* eslint-disable no-console */
/* eslint-disable no-unused-vars */

let ride = {
    name: "The Simpsons",
    excitementLevel: 100,
    capacity: 20,
    get go() {
        console.log("go");
    }
};

let descriptors = Object.getOwnPropertyDescriptors(ride);
console.log(descriptors);

descriptors.go.get();
console.log(descriptors.capacity.value);

let descriptor = Object.getOwnPropertyDescriptor(ride, "go");
