/* eslint-disable no-console */

let ride = {
    name: "The Simpsons",
    excitementLevel: 100,
    capacity: 20,
    get go() {
        console.log("go");
    }
};

let copy = {};
Object.assign(copy, ride);
let descriptor = Object.getOwnPropertyDescriptor(copy, 'go');

let aBetterCopy = {};
Object.defineProperties(aBetterCopy,
    Object.getOwnPropertyDescriptors(ride));
let betterDescriptor =
    Object.getOwnPropertyDescriptor(aBetterCopy, 'go');

let clone = Object.create(Object.getPrototypeOf(ride),
    Object.getOwnPropertyDescriptors(ride));
