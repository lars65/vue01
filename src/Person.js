export default class Person {
  constructor(name) {
    this.name = name;
  }
}

var auteur = {
  naam: 'Lars',
  fami: 'Devocht',
  boek: {
    titel: 'Javascript programming',
    isbn: 929292992,
    pages: 200
  }
};

console.log(auteur.boek);

/* instance this property versus instance prototype property */

 function f() {
   this.p = function() {
     return 111;
   };
 }
 f.prototype.x = function() {
   return 222;
 };


// var ff = new f();
// console.log(ff.x());
//
// var gg = {
//   name: "xxx",
//   ff: function () {
//     return this; // this bound to enclosing object
//   }
// };
// console.log(gg.ff());
//
// var member = 'global';
//
// function hh () {
//   this.member = 'local'; // this in constructor is bound to object being constructed
// };
//
// var hhx = new hh();
// console.log(hh.member);
