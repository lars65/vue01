import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import qs from 'qs'

new Vue({
  el: '#app',
  render: h => h(App)
})

const helloWorld = () => {
  console.log("Arrow functions are working");
};
helloWorld();

(function() {
  alert('{ "recipient": "lars.devochtgmail.com", "subject": "test", "message": "test message" }');
  var output = document.getElementById('output');
  document.getElementById('post').onclick = function() {
    var textdata = document.getElementById('data').value;
    var obj = JSON.parse(textdata);
    axios.post('/requests',qs.stringify(obj), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function(res) {
      output.className = 'container';
      output.innerHTML = res.data;
    }).catch(function(err) {
      output.className = 'container text-danger';
      output.innerHTML = err.message;
      console.log(obj);
    });
  };
})();
