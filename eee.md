
[![N-VA Lokeren](http://lokeren.n-va.be/sites/all/themes/nva_afdelingen/logo.png)](https://lokeren.n-va.be/)
![cat](https://lokeren.n-va.be/sites/all/themes/nva_afdelingen/logo.png)


Bla bla <sup id="a1">[1](#f1)</sup>
Then from within the footnote, link back to it.
Bla bla <sup id="a2">[2](#f2)</sup>
Then from within the footnote, link back to it.
Bla bla <sup id="a3">[3](#f3)</sup>
Then from within the footnote, link back to it.

<b id="f1">1</b> Footnote content here. [↩](#a1)
<b id="f2">2</b> Footnote content here. [↩](#a2)
<b id="f3">3</b> Footnote content here. [↩](#a3)
