curl 'https://transfer.const-court.be/requests'
-H 'Cookie: auth_token=dF4KVfQY1XCyUXdqOEbLCh; _filetransfer_session=7d897db92cc997a27098de50f983aac4'
-H 'Origin: https://transfer.const-court.be'
-H 'Accept-Encoding: gzip, deflate, br'
-H 'Accept-Language: nl'
-H 'Upgrade-Insecure-Requests: 1'
-H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
-H 'Content-Type: application/x-www-form-urlencoded'
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
-H 'Cache-Control: max-age=0' -H 'Referer: https://transfer.const-court.be/message/new'
-H 'Connection: keep-alive'
--data 'utf8=%E2%9C%93&authenticity_token=iI0SiayQpd%2FNaawR772zkp3eUsTEuiYP3eioBBHGYp8%3D&request%5Brecipient%5D=lars.devocht%40gmail.com&request%5Bsubject%5D=xxx+lars.devocht%40gmail.com&request%5Bmessage%5D=bericht+bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb&request%5Bbcc_myself%5D=0'
--compressed
unescaped version:
utf8=â&authenticity_token=iI0SiayQpd/NaawR772zkp3eUsTEuiYP3eioBBHGYp8=&request[recipient]=lars.devocht@gmail.com&request[subject]=xxx+lars.devocht@gmail.com&request[message]=bericht+bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb&request[bcc_myself]=0
json versie:
{
  "utf8": "â",
  "authenticity_token": "iI0SiayQpd/NaawR772zkp3eUsTEuiYP3eioBBHGYp8=",
  "request[recipient]": "lars.devocht@gmail.com",
  "request[subject]": "xxx+lars.devocht@gmail.com",
  "request[message]": "bericht+bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
  "request[bcc_myself]": "0'\r\n"
}

curl 'https://transfer.const-court.be/users'
-H 'Cookie: auth_token=dF4KVfQY1XCyUXdqOEbLCh; _filetransfer_session=7d897db92cc997a27098de50f983aac4' -H 'Origin: https://transfer.const-court.be' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: nl' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
-H 'Content-Type: application/x-www-form-urlencoded'
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
-H 'Cache-Control: max-age=0'
-H 'Referer: https://transfer.const-court.be/message/new'
-H 'Connection: keep-alive'
--data 'utf8=%E2%9C%93&authenticity_token=iI0SiayQpd%2FNaawR772zkp3eUsTEuiYP3eioBBHGYp8%3D&user%5Bname%5D=nnnnnnnnnnnnnnnnnnnnnaaaaaaaame&user%5Bemail%5D=eeeeeeeeeeeeeeeeee%40mail.be&user%5Bmessage%5D=mmmmmmmmmmmmmmmmmesssss&user%5Bbcc_myself%5D=0'
--compressed

unescaped version:
http://www.utilities-online.info/urlencode/
utf8=â&authenticity_token=iI0SiayQpd/NaawR772zkp3eUsTEuiYP3eioBBHGYp8=&user[name]=nnnnnnnnnnnnnnnnnnnnnaaaaaaaame&user[email]=eeeeeeeeeeeeeeeeee@mail.be&user[message]=mmmmmmmmmmmmmmmmmesssss&user[bcc_myself]=0
JSON formatted:
https://devutilsonline.com/json/convert-http-query-string-to-json
{
  "utf8": "â",
  "authenticity_token": "iI0SiayQpd/NaawR772zkp3eUsTEuiYP3eioBBHGYp8",
  "user[name]": "nnnnnnnnnnnnnnnnnnnnnaaaaaaaame",
  "user[email]": "lars.devocht@gmail.com",
  "user[message]": "mmmmmmmmmmmmmmmmmesssss",
  "user[bcc_myself]": "0"
}

curl 'https://transfer.const-court.be/admin/configuration/upload_asset'
-H 'Cookie: auth_token=dF4KVfQY1XCyUXdqOEbLCh; _filetransfer_session=7d897db92cc997a27098de50f983aac4'
-H 'Origin: https://transfer.const-court.be'
-H 'Accept-Encoding: gzip, deflate, br'
-H 'Accept-Language: nl'
-H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
-H 'Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryRd4X1BklsFtChnSG'
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
-H 'Cache-Control: max-age=0'
-H 'Referer: https://transfer.const-court.be/admin/configuration/branding'
-H 'Connection: keep-alive'

--data-binary
$'------WebKitFormBoundaryRd4X1BklsFtChnSG\r\nContent-Disposition:
form-data;
name="utf8"\r\n\r\n\u2713\r\n------WebKitFormBoundaryRd4X1BklsFtChnSG\r\nContent-Disposition:
form-data;
name="authenticity_token"\r\n\r\niI0SiayQpd/NaawR772zkp3eUsTEuiYP3eioBBHGYp8=\r\n------WebKitFormBoundaryRd4X1BklsFtChnSG\r\nContent-Disposition:
form-data;
name="asset"; filename="build.js"\r\nContent-Type: application/javascript\r\n\r\n\r\n------WebKitFormBoundaryRd4X1BklsFtChnSG\r\nContent-Disposition:
form-data;
name="commit"\r\n\r\nUpload\r\n------WebKitFormBoundaryRd4X1BklsFtChnSG--\r\n

-----------------------------7602142224047
Content-Disposition: form-data; name="utf8"

✓
-----------------------------7602142224047
Content-Disposition: form-data; name="authenticity_token"

brpaG+Znr6hPOSJ2jAKSfvatZMWf6Tag4G/maLZFfA0=
-----------------------------7602142224047
Content-Disposition: form-data; name="asset"; filename="index.html"
Content-Type: text/html

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>lars-project</title>
  </head>
  <body>
    <div id="app"></div>
  <script type="text/javascript" src="/img/build.js"></script></body>
</html>
-----------------------------7602142224047
Content-Disposition: form-data; name="commit"

Upload
-----------------------------7602142224047--

via command line werkt dit nog niet met CURL:
curl -v -b cookies.txt -F utf8=â -F commit=Upload -F asset=@wallaby.js -F authenticity_token=brpaG+Znr6hPOSJ2jAKSfvatZMWf6Tag4G/maLZFfA0= -u "lars.devocht@const-court.be:Leer261!" https://transfer.const-court.be/admin/configuration/upload_asset
 of met deze auth ?  --user pBdmnQ8De4eGLJKlUfvtnn:x

--cookie-jar

curl -v -c cookies.txt https://transfer.const-court.be

curl -c cookies.txt "https://transfer.const-court.be/login" -H "Host: transfer.const-court.be" -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" -H "Accept-Language: nl,en-US;q=0.7,en;q=0.3" --compressed -H "Referer: https://transfer.const-court.be/" -H "Content-Type: application/x-www-form-urlencoded" -H "Cookie: _filetransfer_session=b0c3f94d252c8cebce7365e3028a1602" -H "Connection: keep-alive" -H "Upgrade-Insecure-Requests: 1"
--data "utf8="%"E2"%"9C"%"93&authenticity_token=pRUigqz9gBjM6Hp"%"2FbbcSH"%"2F8VS7kQ07lmqxDeZxMKvGg"%"3D&user"%"5Bemail"%"5D=lars.devocht"%"40const-court.be&user"%"5Bpassword"%"5D=Leer261"%"21&user"%"5Bsave_login"%"5D=0"

de HTTP query geconverteerd naar JSON is dit:
{
  "utf8": "\"%\"E2\"%\"9C\"%\"93",
  "authenticity_token": "pRUigqz9gBjM6Hp\"%\"2FbbcSH\"%\"2F8VS7kQ07lmqxDeZxMKvGg\"%\"3D",
  "user\"%\"5Bemail\"%\"5D": "lars.devocht\"%\"40const-court.be",
  "user\"%\"5Bpassword\"%\"5D": "Leer261\"%\"21",
  "user\"%\"5Bsave_login\"%\"5D": "0"
}

// curl om formData LOGiN te doen (copy/paste van login via webpagina)
content=$(curl -L https://transfer.const-court.be)
=> authenticity_token=bc8Fe0CYw31wRSZzlFos8/sJzg+x/a+YVkGpcC4UM8c=  => deze verandert altijd !!! aahaaaaarch !!!!
curl 'https://transfer.const-court.be/login' -H 'Cookie: _filetransfer_session=15e3a7d84830b6e1bdad8dfabd023544' -H 'Origin: https://transfer.const-court.be' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: nl' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: https://transfer.const-court.be/' -H 'Connection: keep-alive' --data 'utf8=%E2%9C%93&authenticity_token=07ngWhFxhguQ9jplRQh6tpAzN2a9adISpQdNwPpbBRw%3D&user%5Bemail%5D=lars.devocht%40const-court.be&user%5Bpassword%5D=Leer261%21&user%5Bsave_login%5D=0&user%5Bsave_login%5D=1' --compressed
 curl -v -c cookies.txt https://transfer.const-court.be
 => cookie bevat dit:
 _filetransfer_session=ad879227d4b6431e1bd790420e4d3234
 curl 'https://transfer.const-court.be/login' -H 'Cookie: _filetransfer_session=ad879227d4b6431e1bd790420e4d3234' -H 'Origin: https://transfer.const-court.be' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: nl' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: https://transfer.const-court.be/' -H 'Connection: keep-alive' --data 'utf8=%E2%9C%93&authenticity_token=bc8Fe0CYw31wRSZzlFos8/sJzg+x/a+YVkGpcC4UM8c=%3D&user%5Bemail%5D=lars.devocht%40const-court.be&user%5Bpassword%5D=Leer261%21&user%5Bsave_login%5D=0&user%5Bsave_login%5D=1' --compressed

// data die worden gezonden:
utf8=%E2%9C%93&authenticity_token=X3WjD5ENp/XDOAYdidC9SEcRE0/k+qnZBIL902pSb2U=%3D&user%5Bemail%5D=lars.devocht%40const-court.be&user%5Bpassword%5D=Leer261%21&user%5Bsave_login%5D=0&user%5Bsave_login%5D=1

unescaped data parameter:
utf8=â&authenticity_token=X3WjD5ENp/XDOAYdidC9SEcRE0/k+qnZBIL902pSb2U==&user[email]=lars.devocht@const-court.be&user[password]=Leer261!&user[save_login]=0&user[save_login]=1
=>
authenticity_token=X3WjD5ENp/XDOAYdidC9SEcRE0/k+qnZBIL902pSb2U==&user[email]=lars.devocht@const-court.be&user[password]=Leer261!&user[save_login]=0&user[save_login]=1
in JSON ziet het er zo uit:
{
  "utf8": "â",
  "authenticity_token": "X3WjD5ENp/XDOAYdidC9SEcRE0/k+qnZBIL902pSb2U",
  "user[email]": "lars.devocht@const-court.be",
  "user[password]": "Leer261!",
  "user[save_login]": [
    "0",
    "1"
  ]
}

curl https://transfer.const-court.be --cookie-jar cookies.txt | grep csrf

curl 'https://transfer.const-court.be/login' -H 'Cookie: _filetransfer_session=' -H 'Origin: https://transfer.const-court.be' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: nl' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36' -H 'Content-Type:application/x-www-form-urlencoded' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: https://transfer.const-court.be/' -H 'Connection: keep-alive' --data 'utf8=%E2%9C%93&authenticity_token==%3D&user%5Bemail%5D=lars.devocht%40const-court.be&user%5Bpassword%5D=Leer261%21&user%5Bsave_login%5D=0&user%5Bsave_login%5D=1' --compressed

4:
authToken=`curl -s -L --cookie-jar cookies.txt 'https://transfer.const-court.be/login' | grep 'csrf-token' | cut -c 16-59`
echo $authToken
authToken=`curl -s -L -c cookies.txt -b cookies.txt --data 'user[email]=lars.devocht@const-court.be&user[password]=Leer261!&user[save_login]=0&user[save_login]=1' --data-urlencode authenticity_token=$authToken 'https://transfer.const-court.be/login' | grep 'csrf-token' | cut -c 16-59`
echo $authToken
===================> op dit moment "session initiated" !

curl -s -L -c cookies.txt -b cookies.txt --header 'Content-Type:application/x-www-form-urlencoded' --data 'user[email]=lars.devocht@const-court.be&user[password]=Leer261!&user[save_login]=0&user[save_login]=1' --data-urlencode authenticity_token=$authToken 'https://transfer.const-court.be/login' | grep 'csrf-token' | cut -c 16-59
authToken=`curl -s -L -c cookies.txt -b cookies.txt --header 'Content-Type:application/x-www-form-urlencoded' --data 'user[email]=lars.devocht@const-court.be&user[password]=Leer261!&user[save_login]=0&user[save_login]=1' --data-urlencode authenticity_token=$authToken 'https://transfer.const-court.be/login' | grep 'csrf-token' | cut -c 16-59`
echo $authToken
=> dan dit, zonder -c cookie:
curl -s -L -b cookies.txt --header 'Content-Type:application/x-www-form-urlencoded' --data 'user[email]=lars.devocht@const-court.be&user[password]=Leer261!&user[save_login]=0&user[save_login]=1' --data-urlencode authenticity_token=$authToken 'https://transfer.const-court.be/login' | grep 'csrf-token' | cut -c 16-59
====== doen een terminate !! aaah !
4:
curl -s -b cookies.txt --form 'commit=Upload' --form 'asset=@build.js' --form authenticity_token=$authToken https://transfer.const-court.be/admin/configuration/upload_asset

authToken=`curl -s -L -c cookies.txt -b cookies.txt --header 'Content-Type:application/x-www-form-urlencoded' --data 'user[email]=lars.devocht@const-court.be&user[password]=Leer261!&user[save_login]=0&user[save_login]=1' --data-urlencode authenticity_token=$authToken 'https://transfer.const-court.be/login' | grep 'csrf-token' | cut -c 16-59`
echo $authToken
3:
eerst inloggen, daarna werkt deleten = OK !
curl "https://transfer.const-court.be/admin/configuration/delete_asset?filename=build.js" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" -b cookies.txt --compressed
curl "https://transfer.const-court.be/admin/configuration/delete_asset?filename=index.html" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" -b cookies.txt --compressed

// DEZE SEQUENTIE WERKT:
rm cookies.txt
authToken=`curl -s -L --cookie-jar cookies.txt 'https://transfer.const-court.be/login' | grep 'csrf-token' | cut -c 16-59`
echo $authToken
authToken=`curl -s -L -c cookies.txt -b cookies.txt --data 'user[email]=lars.devocht@const-court.be&user[password]=Leer261!&user[save_login]=0&user[save_login]=1' --data-urlencode authenticity_token=$authToken 'https://transfer.const-court.be/login' | grep 'csrf-token' | cut -c 16-59`
echo $authToken
curl "https://transfer.const-court.be/admin/configuration/delete_asset?filename=build.js" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" -b cookies.txt --compressed
curl -s -b cookies.txt --form 'commit=Upload' --form 'asset=@build.js' --form authenticity_token=$authToken https://transfer.const-court.be/admin/configuration/upload_asset
